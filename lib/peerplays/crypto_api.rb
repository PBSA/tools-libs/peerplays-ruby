module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api/crypto-api Crypto API}
  class CryptoApi < Peerplays::Api
    CRYPTO_API_NAMESPACE = 'crypto'.freeze
    CRYPTO_API_METHODS = %i(
      blind
      blind_sum
      range_get_info
      range_proof_sign
      verify_sum
      verify_range
      verify_range_proof_rewind
    ).freeze
    
    def initialize(options = {})
      @api_name = CRYPTO_API_NAMESPACE
      @methods = CRYPTO_API_METHODS
      
      super
    end
  end
end
