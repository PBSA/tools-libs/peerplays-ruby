# `peerplays-ruby`

Peerplays-ruby the Ruby API for Peerplays blockchain.

## Getting Started

The peerplays-ruby gem is compatible with Ruby 2.4 or later.

### Install the gem for your project

*(Assuming that [Ruby is installed](https://www.ruby-lang.org/en/downloads/) on your computer, as well as [RubyGems](http://rubygems.org/pages/download))*

To install the gem on your computer, run in shell:

```bash
gem install peerplays-ruby
```

... then add in your code:

```ruby
require 'peerplays'
```

To add the gem as a dependency to your project with [Bundler](http://bundler.io/), you can add this line in your Gemfile:

```ruby
gem 'peerplays-ruby', require: 'peerplays'
```

## Get Accounts

```ruby
require 'peerplays'

api = Peerplays::DatabaseApi.new

api.get_accounts(['init1']) do |result|
  result.each do |account|
    puts account
  end
end
```

## Set API Node

To set an alternative node, pass it as an argument, e.g.:

```ruby
api = Peerplays::DatabaseApi.new(url: 'wss://ppyws.roelandp.nl/ws')
```

## Streaming

To start a stream from the last irreversible block:

```ruby
stream = Peerplays::Stream.new

stream.operations do |op_type, op_value|
  puts "#{op_type}: #{op_value}"
end
```

To start a stream from the head block:

```ruby
stream = Peerplays::Stream.new(mode: :head)

stream.operations do |op_type, op_value|
  puts "#{op_type}: #{op_value}"
end
```

To start a stream from a specific block number, pass it as an argument:

```ruby
stream = Peerplays::Stream.new

stream.operations(at_block_num: 9001) do |op_type, op_value|
  puts "#{op_type}: #{op_value}"
end
```

You can also grab the related block number for each operation:

```ruby
stream = Peerplays::Stream.new

stream.operations do |op_type, op_value, block_num|
  puts "#{block_num} :: #{op_type}: #{op_value}"
end
```

To stream only certain operations:

```ruby
stream = Peerplays::Stream.new

stream.operations(types: :transfer) do |op_type, op_value|
  puts "#{op_type}: #{op_value}"
end
```

Or pass an array of certain operations:

```ruby
stream = Peerplays::Stream.new

stream.operations(types: [:account_create, :account_update]) do |op_type, op_value|
  puts "#{op_type}: #{op_value}"
end
```

Or (optionally) just pass the operation(s) you want as the only arguments.  This is semantic sugar for when you want specific types and take all of the defaults.

```ruby
stream = Peerplays::Stream.new

stream.operations(:transfer) do |op_type, op_value|
  puts "#{op_type}: #{op_value}"
end
```

## Reference

For lists of available methods provided by this gem:

* https://www.peerplays.tech/api/peerplays-core-api
* https://www.peerplays.tech/api/peerplays-wallet-api

## Tests

* Clone the client repository into a directory of your choice:
  * `git clone git@gitlab.com:PBSA/PeerplaysIO/tools-libs/ruby-peerplays.git`
* Navigate into the new folder
  * `cd ruby-peerplays`
* All tests can be invoked as follows:
  * `bundle exec rake test`
* To run `testnet` tests (which does actual broadcasts)
  * `TEST_NODE=ws://api.ppy-beatrice.blckchnd.com bundle exec rake`

## Contributions

Patches are welcome! Contributors are listed in the `peerplays-ruby.gemspec` file. Please run the tests (`rake test`) before opening a pull request and make sure that you are passing all of them. If you would like to contribute, but don't know what to work on, check the issues list.

## Issues

When you find issues, please report them!

## License

MIT
