module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api/database-api Database API}
  class DatabaseApi < Peerplays::Api
    DATABASE_API_NAMESPACE = 'database'
    DATABASE_API_METHODS = %i(
      get_objects
      set_subscribe_callback
      set_pending_transaction_callback
      set_block_applied_callback
      cancel_all_subscriptions
      get_block_header
      get_block
      get_transaction
      get_recent_transaction_by_id
      get_chain_properties
      get_global_properties
      get_config
      get_chain_id
      get_dynamic_global_properties
      get_key_references
      get_accounts
      get_full_accounts
      get_account_by_name
      get_account_references
      lookup_account_names
      lookup_accounts
      get_account_count
      get_account_balances
      get_named_account_balances
      get_balance_objects
      get_vested_balances
      get_vesting_balances
      get_assets
      list_assets
      lookup_asset_symbols
      get_order_book
      get_limit_orders
      get_call_orders
      get_settle_orders
      get_margin_positions
      subscribe_to_market
      unsubscribe_from_market
      get_ticker
      get_24_volume
      get_trade_history
      get_witnesses
      get_witness_by_account
      lookup_witness_accounts
      get_witness_count
      get_committee_members
      get_committee_member_by_account
      lookup_committee_member_accounts
      get_workers_by_account
      lookup_vote_ids
      get_transaction_hex
      get_required_signatures
      get_potential_signatures
      get_potential_address_signatures
      verify_authority
      verify_account_authority
      validate_transaction
      get_required_fees
      get_proposed_transactions
      get_blinded_balances
    ).freeze
      
    def initialize(options = {})
      @api_name = DATABASE_API_NAMESPACE
      @methods = DATABASE_API_METHODS
      
      super
    end
  end
end
