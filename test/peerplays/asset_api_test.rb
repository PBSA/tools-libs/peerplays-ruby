require 'test_helper'

module Peerplays
  class AssetApiTest < Peerplays::Test
    def setup
      @api = Peerplays::AssetApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_get_asset_holders_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_asset_holders()
      end
    end
    
    def test_get_asset_holders
      options = [
        'PPY', # string asset
        0, # uint32_t start
        1 # uint32_t limit
      ]
      
      @api.get_asset_holders(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
  end
end
