module Peerplays
  # See: {https://www.peerplays.tech/api/peerplays-core-api/block-api Block API}
  class BlockApi < Peerplays::Api
    BLOCK_API_NAMESPACE = 'block'.freeze
    BLOCK_API_METHODS = %i(
      get_blocks
    ).freeze
    
    def initialize(options = {})
      @api_name = BLOCK_API_NAMESPACE
      @methods = BLOCK_API_METHODS
      
      super
    end
  end
end
