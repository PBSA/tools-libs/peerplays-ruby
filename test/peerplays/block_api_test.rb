require 'test_helper'

module Peerplays
  class BlockApiTest < Peerplays::Test
    def setup
      @api = Peerplays::BlockApi.new(url: TEST_NODE, check_enabled: true) rescue skip('not enabled')
    end
    
    def test_method_missing
      assert_raises NoMethodError do
        @api.bogus
      end
    end
    
    def test_get_blocks_empty
      assert_raises Peerplays::ArgumentError do
        @api.get_blocks()
      end
    end
    
    def test_get_blocks
      options = [
        1, # uint32_t block_num_from
        2 # uint32_t block_num_to
      ]
      
      @api.get_blocks(*options) do |result|
        assert_equal Hashie::Array, result.class
        assert_equal 1, result.size
      end
    end
  end
end
